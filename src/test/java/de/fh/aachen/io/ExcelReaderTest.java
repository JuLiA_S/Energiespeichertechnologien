package de.fh.aachen.io;

import de.fh.aachen.data.SimulationDTO;
import de.fh.aachen.data.ValueWithUnit;
import de.fh.aachen.timeseries.TimeseriesUnit;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.fail;

/**
 *
 */
public class ExcelReaderTest {

    private final String filepath = "src/test/resources/Eingangsdaten_Programieraufgabe_EST_SS2017.xlsx";
    Workbook workbook;

    @BeforeEach
    public void setUp(){
        try {
            workbook = new XSSFWorkbook(filepath);
        } catch (IOException e) {
            fail("unable to run this test. " + e.getMessage());
        }
    }

    @Test
    public void readBatteryData(){
        final SimulationDTO dto = new SimulationDTO();
        ExcelReader.readBatteryData(workbook, dto);

        assertThat(dto.getBatterysize(), is(new ValueWithUnit(BigDecimal.valueOf(2), TimeseriesUnit.KILO_WATT_STUNDE)));
        assertThat("usable capacity", dto.getUsableCapacity(), is(new BigDecimal("0.9")));
        assertThat("charge efficiency", dto.getChargeEfficiency(), is(new BigDecimal("0.98")));
        assertThat("discharge efficiency", dto.getDischargeEfficiency(), is(new BigDecimal("0.97")));
    }
}
