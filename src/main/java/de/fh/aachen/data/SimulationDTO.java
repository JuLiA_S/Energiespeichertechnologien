package de.fh.aachen.data;

import de.fh.aachen.timeseries.Timeseries;

import java.math.BigDecimal;

/**
 *
 */
public class SimulationDTO {
    private Timeseries pvProduction_scaled;
    private Timeseries consumption;

    private ValueWithUnit batterysize;
    private BigDecimal usableCapacity;
    private BigDecimal chargeEfficiency;
    private BigDecimal dischargeEfficiency;

    public SimulationDTO() {
    }

    public Timeseries getPvProduction_scaled() {
        return pvProduction_scaled;
    }

    public SimulationDTO setPvProduction_scaled(Timeseries pvProduction_scaled) {
        this.pvProduction_scaled = pvProduction_scaled;
        return this;
    }

    public Timeseries getConsumption() {
        return consumption;
    }

    public SimulationDTO setConsumption(Timeseries consumption) {
        this.consumption = consumption;
        return this;
    }

    public ValueWithUnit getBatterysize() {
        return batterysize;
    }

    public SimulationDTO setBatterysize(ValueWithUnit batterysize) {
        this.batterysize = batterysize;
        return this;
    }

    public BigDecimal getUsableCapacity() {
        return usableCapacity;
    }

    public SimulationDTO setUsableCapacity(BigDecimal usableCapacity) {
        this.usableCapacity = usableCapacity;
        return this;
    }

    public BigDecimal getChargeEfficiency() {
        return chargeEfficiency;
    }

    public SimulationDTO setChargeEfficiency(BigDecimal chargeEfficiency) {
        this.chargeEfficiency = chargeEfficiency;
        return this;
    }

    public BigDecimal getDischargeEfficiency() {
        return dischargeEfficiency;
    }

    public SimulationDTO setDischargeEfficiency(BigDecimal dischargeEfficiency) {
        this.dischargeEfficiency = dischargeEfficiency;
        return this;
    }
}
