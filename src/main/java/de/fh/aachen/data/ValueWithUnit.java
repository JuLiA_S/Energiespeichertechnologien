package de.fh.aachen.data;

import de.fh.aachen.timeseries.TimeseriesUnit;

import java.math.BigDecimal;

/**
 *
 */
public class ValueWithUnit {

    private BigDecimal value;
    private TimeseriesUnit unit;

    public ValueWithUnit(BigDecimal value, TimeseriesUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public TimeseriesUnit getUnit() {
        return unit;
    }

    public void setUnit(TimeseriesUnit unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValueWithUnit)) return false;

        ValueWithUnit that = (ValueWithUnit) o;

        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        return unit == that.unit;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ValueWithUnit{" +
                "value=" + value +
                ", unit=" + unit +
                '}';
    }
}
