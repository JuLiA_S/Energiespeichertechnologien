package de.fh.aachen.timeseries;

import de.fh.aachen.exception.DifferentResolutionException;
import de.fh.aachen.exception.DifferentUnitsException;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.Period;
import java.util.function.BiFunction;

/**
 * Class to represent a timeseries object.
 * A timeseries object contains  values, unit and period.
 * The period has to cover less than a day!
 */
public abstract class Timeseries {

    protected final Duration period;
    protected final TimeseriesUnit unit;

    protected Timeseries(Duration period, TimeseriesUnit unit) {
        this.period = period;
        this.unit = unit;
    }

    public abstract BigDecimal getValue(Instant instant);

    public Duration getPeriod() {
        return period;
    }

    public TimeseriesUnit getUnit() {
        return unit;
    }

    public Timeseries add(Timeseries timeseries){
        return new FormulaTimeseries(period, unit, instant -> {
            checkUnits(this, timeseries, "timeseries should have the same unit for addition");
            checkPeriods(this, timeseries, "timeseries should have the same resolution for addition");

            final BigDecimal v1 = getValue(instant);
            final BigDecimal v2 = timeseries.getValue(instant);

            if(v1 == null) return v2;
            if(v2 == null) return v1;
            return v1.add(v2);
        });
    }

    public Timeseries subtract(Timeseries timeseries){
        return new FormulaTimeseries(period, unit, instant -> {
            checkUnits(this, timeseries, "timeseries should have the same unit for subtraction");
            checkPeriods(this, timeseries, "timeseries should have the same resolution for subtraction");

            BigDecimal v1 = getValue(instant);
            final BigDecimal v2 = timeseries.getValue(instant);

            if(v1 == null && v2 == null) return null;
            if(v1 == null) v1 = BigDecimal.ZERO;
            if(v2 == null) return v1;
            return v1.subtract(v2);
        });
    }

    public Timeseries multiply(Timeseries timeseries){
        return new FormulaTimeseries(period, unit, instant -> {
            checkUnits(this, timeseries, "timeseries should have the same unit for multiplication");
            checkPeriods(this, timeseries, "timeseries should have the same resolution for multiplication");

            final BigDecimal v1 = getValue(instant);
            final BigDecimal v2 = timeseries.getValue(instant);

            if(v1 == null || v2 == null) return null;
            return v1.multiply(v2);
        });
    }

    public Timeseries divide(Timeseries timeseries){
        return new FormulaTimeseries(period, unit, instant -> {
            checkUnits(this, timeseries, "timeseries should have the same unit for division");
            checkPeriods(this, timeseries, "timeseries should have the same resolution for division");

            final BigDecimal v1 = getValue(instant);
            final BigDecimal v2 = timeseries.getValue(instant);

            if(v1 == null || v2 == null) return null;
            return v1.divide(v2);
        });
    }

    public Timeseries toUnit(TimeseriesUnit newUnit){
        Timeseries result = this;
        if(unit.isIntegral() != newUnit.isIntegral()){
            final BigDecimal secondsOfOneHour = BigDecimal.valueOf(3600);
            final BigDecimal secondsOfPeriod = BigDecimal.valueOf(period.getSeconds());
            final BigDecimal factor;
            if(unit.isIntegral()){
                //Wh -> W
                //*3600/300 (if period is 5 Minutes)
                factor = secondsOfOneHour.divide(secondsOfPeriod);
            }else {
                //W -> Wh
                //*300/3600 (if period is 5 Minutes)
                factor = secondsOfOneHour.divide(secondsOfPeriod);
            }
            result = result.multiply(new ConstantValueTimeseries(period, unit, factor));
        }

        //both units are integral or not
        //we have only to multiply a fix factor
        final int conversionExponent = unit.getExponent() - newUnit.getExponent();
        if(conversionExponent != 0){
            result = result.multiply(new ConstantValueTimeseries(period, unit, BigDecimal.TEN.pow(conversionExponent)));
        }

        return result;
    }

    protected void checkUnits(Timeseries ts1, Timeseries ts2, String message){
        if(!ts1.getUnit().equals(ts2.getUnit())){
            throw new DifferentUnitsException(message);
        }
    }

    protected void checkPeriods(Timeseries ts1, Timeseries ts2, String message){
        if(!ts1.getPeriod().equals(ts2.getPeriod())){
            throw new DifferentResolutionException(message);
        }
    }
}
