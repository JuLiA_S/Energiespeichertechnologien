package de.fh.aachen.timeseries;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class SourceTimeseries extends Timeseries {

    private final Map<Instant, BigDecimal> values;

    public SourceTimeseries(Duration period, TimeseriesUnit unit) {
        super(period, unit);
        this.values = new HashMap();
    }

    public SourceTimeseries(Duration period, TimeseriesUnit unit,Map<Instant, BigDecimal> values) {
        super(period, unit);
        this.values = values;
    }

    @Override
    public BigDecimal getValue(Instant instant) {
        return values.get(instant);
    }

    public void setValue(Instant instant, BigDecimal value){
        values.put(instant, value);
    }
}
