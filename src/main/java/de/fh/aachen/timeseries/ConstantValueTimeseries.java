package de.fh.aachen.timeseries;


import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;

/**
 * This class is used for example to multiply a timerseries with a constant factor.
 */
public class ConstantValueTimeseries extends Timeseries {

    private final BigDecimal value;

    public ConstantValueTimeseries(Duration period, TimeseriesUnit unit, BigDecimal value) {
        super(period, unit);
        this.value = value;
    }

    @Override
    public BigDecimal getValue(Instant instant) {
        return value;
    }
}
