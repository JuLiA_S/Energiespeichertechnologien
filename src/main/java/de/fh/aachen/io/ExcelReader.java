package de.fh.aachen.io;

import de.fh.aachen.data.SimulationDTO;
import de.fh.aachen.data.ValueWithUnit;
import de.fh.aachen.timeseries.TimeseriesUnit;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * class to read all necessary data from excel
 */
public class ExcelReader {

    public static SimulationDTO readFile(String file) throws IOException {
        final SimulationDTO dto = new SimulationDTO();

        final Workbook workbook = new XSSFWorkbook(file);
        readBatteryData(workbook, dto);
        //TODO read consumption and production data

        return dto;
    }

    //package local for testing issues
    static void readBatteryData(Workbook workbook, SimulationDTO dto) {
        final Sheet sheet = workbook.getSheetAt(0);
        {
            final Row row = sheet.getRow(11);
            final Cell batterysizeCell = row.getCell(1);
            final double value = batterysizeCell.getNumericCellValue();
            final String formatString = batterysizeCell.getCellStyle().getDataFormatString(); //##,##0.0\ "  kWh"
            final TimeseriesUnit unit = TimeseriesUnit.parse(formatString); //TODO extract unit symbol from format string

            dto.setBatterysize(new ValueWithUnit(new BigDecimal(value), unit));
        }
        {
            final Row row = sheet.getRow(12);
            final Cell usableCapacityCell = row.getCell(1);
            final double usableCapacity = usableCapacityCell.getNumericCellValue(); // 90.0
            dto.setUsableCapacity(BigDecimal.valueOf(usableCapacity).divide(BigDecimal.valueOf(100))); //Wert in %
        }
        {
            final Row row = sheet.getRow(13);
            final Cell chargeEfficiencyCell = row.getCell(1);
            final double chargeEfficiency = chargeEfficiencyCell.getNumericCellValue(); // 98.0
            dto.setChargeEfficiency(BigDecimal.valueOf(chargeEfficiency).divide(BigDecimal.valueOf(100))); //Wert in %
        }
        {
            final Row row = sheet.getRow(14);
            final Cell dischargeEfficiencyCell = row.getCell(1);
            final double dischargeEfficiency = dischargeEfficiencyCell.getNumericCellValue(); // 97.0
            dto.setDischargeEfficiency(BigDecimal.valueOf(dischargeEfficiency).divide(BigDecimal.valueOf(100))); //Wert in %
        }
    }
}
