package de.fh.aachen.exception;

/**
 *
 */
public class DifferentResolutionException extends RuntimeException {

    public DifferentResolutionException() {
    }

    public DifferentResolutionException(String message) {
        super(message);
    }
}
