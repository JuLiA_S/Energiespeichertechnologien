package de.fh.aachen.exception;

/**
 *
 */
public class DifferentUnitsException extends RuntimeException {

    public DifferentUnitsException() {
        super();
    }

    public DifferentUnitsException(String message) {
        super(message);
    }
}
