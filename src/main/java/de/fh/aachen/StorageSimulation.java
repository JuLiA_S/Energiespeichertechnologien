package de.fh.aachen;

import de.fh.aachen.timeseries.Timeseries;

/**
 * class to simulate energy storage usage
 */
public class StorageSimulation {

    /**
     * Zeitreihe, die die erzeugte Energie der PV-Anlage beschreibt
     */
    private Timeseries pvErzeugung;
    /**
     * Zeitreihe, die den benoetigten/verbrauchten Strom beschreibt
     */
    private Timeseries verbrauch;

    /**
     * Zeitreihe, die den Ladezustand des Speichers beschreibt
     */
    private Timeseries speicherverlauf;
    /**
     * Zeitreihe, die die eingespeiste Energie beschreibt
     */
    private Timeseries einspeisung;

}
